﻿using game_of_goose.Business.Dice;
using game_of_goose.Business.Factories;
using game_of_goose.Business.Factories.Interface;
using game_of_goose.Business.GameState;
using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;
using game_of_goose.Business.Services;
using game_of_goose.Business.Tiles;
using Moq;

namespace game_of_goose.Business.Tests.TileTests
{
    public class DeathTests
    {
        [Test]
        public void WhenLandingOnDeathTile58_ThenPlayerGoesToStartPosition0()
        {
            //Arrange
            Mock<ILogger> logger = new Mock<ILogger>();
            ITileFactory tileFactory = new TileFactory(logger.Object);
            List<ITile> tileList = new List<ITile>();
            Mock<IPlayerFactory> playerFactory = new Mock<IPlayerFactory>();
            Mock<IUserInteraction> userInteraction = new Mock<IUserInteraction>();
            Mock<IDice> dice = new Mock<IDice>();
            Mock<List<IPlayer>> playerList = new Mock<List<IPlayer>>();
            Mock<IValidationService> validService = new Mock<IValidationService>();
            IGame game = new Game(tileList, tileFactory, logger.Object, playerFactory.Object, userInteraction.Object, dice.Object, playerList.Object, validService.Object);
            IPlayer player = new Player(game, "Jim", logger.Object);
            player.Position = 51;
            int[] diceRollValue = [3, 4];
            int expectedResult = 0;

            //Act
            player.Move(diceRollValue);

            //Assert
            Assert.That(expectedResult, Is.EqualTo(player.Position));
        }
    }
}
