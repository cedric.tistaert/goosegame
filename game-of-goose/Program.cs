﻿using game_of_goose.Business.GameState;
using Microsoft.Extensions.DependencyInjection;

namespace game_of_goose
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            ServiceCollection serviceCollection = new Startup().RegisterServices();
            ServiceProvider provider = serviceCollection.BuildServiceProvider();
            IGame game = provider.GetService<IGame>();
            game.Welcome();
            game.AskHowManyPlayers();
            game.PlayGame();
            game.ThanksForPlaying();
        }
    }
}
