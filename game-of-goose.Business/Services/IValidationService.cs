﻿using game_of_goose.Business.Logging;

namespace game_of_goose.Business.Services
{
    public interface IValidationService
    {
        int ValidatePlayerAmount(int playersAmount, IUserInteraction userInteraction, ILogger logger);
        string ValidatePlayerName(string playerName, IUserInteraction userInteraction, ILogger logger);
    }
}
