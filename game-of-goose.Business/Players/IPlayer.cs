﻿namespace game_of_goose.Business.Players
{
    public interface IPlayer
    {
        int Position { get; set; }
        int Turn { get; set; }
        int[] DiceRolls { get; set; }
        string Name { get; set; }
        bool StuckInWell { get; set; }
        bool OppositeDirection { get; set; }
        void AddTurnCount(int turns);
        void Move(int[] diceRolls);
        void MoveTo(int v);
        void WinsGame(IPlayer player);


    }
}
