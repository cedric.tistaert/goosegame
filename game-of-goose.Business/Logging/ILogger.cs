﻿namespace game_of_goose.Business.Logging
{
    public interface ILogger
    {
        void Log(string message);
        void LogEmptyLine();
        void LogError(string message);
    }
}
