﻿using game_of_goose.Business.GameState;
using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;

namespace game_of_goose.Business.Factories.Interface
{
    public interface IPlayerFactory
    {
        IPlayer Create(string playerName, IGame game, ILogger logger);
    }
}
