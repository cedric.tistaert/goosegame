﻿using game_of_goose.Business.Dice;
using game_of_goose.Business.Factories.Interface;
using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;
using game_of_goose.Business.Services;
using game_of_goose.Business.Tiles;
using game_of_goose.Business.Tiles.Enum;

namespace game_of_goose.Business.GameState
{
    public class Game : IGame
    {
        private List<ITile> _gameBoard;
        private ITileFactory _tileFactory;
        private ILogger _logger;
        private IPlayerFactory _playerFactory;
        private IUserInteraction _userInteraction;
        private IDice _dice;
        private IValidationService _validationService;
        private int _turns;
        private List<IPlayer> _currentPlayers;

        public Game(List<ITile> gameboard, ITileFactory tileFactory, ILogger logger, IPlayerFactory playerFactory, IUserInteraction userInteraction, IDice dice, List<IPlayer> playerList, IValidationService validationService)
        {
            _gameBoard = gameboard;
            _tileFactory = tileFactory;
            _logger = logger;
            _playerFactory = playerFactory;
            _userInteraction = userInteraction;
            _dice = dice;
            _turns = 0;
            _currentPlayers = playerList;
            _validationService = validationService;
            BoardSize = 63;
            CreateBoard();
        }
        public int BoardSize { get; set; }
        public bool GameIsWon { get; set; }
        public void GameWon(IPlayer player)
        {
            _logger.Log($"{player.Name} is gewonnen!");
            _logger.Log("YoU're WiNnEr.");
            GameIsWon = true;
        }
        public void CreateBoard()
        {
            for (int i = 0; i <= BoardSize; i++)
            {
                if (i == 6)
                {
                    _gameBoard.Add(_tileFactory.Create(i, TileType.Bridge));
                }
                else if (i == 19)
                {
                    _gameBoard.Add(_tileFactory.Create(i, TileType.Inn));

                }
                else if (i == 31)
                {
                    _gameBoard.Add(_tileFactory.Create(i, TileType.Well));
                }
                else if (i == 42)
                {
                    _gameBoard.Add(_tileFactory.Create(i, TileType.Maze));
                }
                else if (i == 52)
                {
                    _gameBoard.Add(_tileFactory.Create(i, TileType.Prison));
                }
                else if (i == 58)
                {
                    _gameBoard.Add(_tileFactory.Create(i, TileType.Death));
                }
                else if (i == 63)
                {
                    _gameBoard.Add(_tileFactory.Create(i, TileType.End));
                }
                else if (i == 5 || i == 9 || i == 14 || i == 18 || i == 23 || i == 18 || i == 27 || i == 32 || i == 36 || i == 41 || i == 45 || i == 50 || i == 54 || i == 59)
                {
                    _gameBoard.Add(_tileFactory.Create(i, TileType.Goose));
                }
                else
                {
                    _gameBoard.Add(_tileFactory.Create(i, TileType.RegularTile));
                }
            }
        }
        public void AskHowManyPlayers()
        {
            _logger.Log("Met hoeveel spelers wilt u spelen? Maximaal 4 spelers.");
            int playersAmount = 0;
            playersAmount = _validationService.ValidatePlayerAmount(playersAmount, _userInteraction, _logger);
            CreatePlayers(playersAmount);
        }
        private void CreatePlayers(int playersAmount)
        {
            for (int i = 1; i <= playersAmount; i++)
            {
                _logger.LogEmptyLine();
                _currentPlayers.Add(CreatePlayer(i));
            }
        }
        public ITile GetTile(int destination)
        {
            ITile tileToFetch = _gameBoard[destination];
            return tileToFetch;
        }
        public IPlayer CreatePlayer(int spelerNummer)
        {
            _logger.Log($"Speler {spelerNummer} geef uw naam in");
            string playerName = "";
            playerName = _validationService.ValidatePlayerName(playerName, _userInteraction, _logger);
            IPlayer player = _playerFactory.Create(playerName, this, _logger);
            return player;
        }
        public void PlayRound(IPlayer player)
        {
            if (player.Turn == _turns)
            {
                player.Move(_dice.RollTheDices());
                player.AddTurnCount(1);
                _logger.LogEmptyLine();
            }
            else
            {
                _logger.Log($"{player.Name} slaagt hun beurt over!");
            }
            _userInteraction.MakeUserPressEnter();
            _userInteraction.ClearScreen();
        }
        public void PlayGame()
        {
            do
            {
                foreach (var player in _currentPlayers)
                {
                    _userInteraction.ClearScreen();
                    ShowPositions();
                    PlayRound(player);
                    if (GameIsWon == true)
                    {
                        break;
                    }
                }
                _turns++;
            }
            while (!GameIsWon);
        }
        public void ThanksForPlaying()
        {
            ShowPositions();
            _logger.Log("Thank you for playing the Goose Game!");
            _logger.Log("⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿\r\n⣿⣿⣿⣿⣿⣿⣿⣿⡿⢋⣩⣭⣶⣶⣮⣭⡙⠿⣿⣿⣿⣿⣿⣿\r\n⣿⣿⣿⣿⣿⣿⠿⣋⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡙⢿⣿⣿⣿\r\n⣿⣿⣿⣿⣿⡃⠄⠹⡿⣿⣿⣿⣿⠟⠛⣿⣿⣿⣿⣷⡌⢿⣿⣿\r\n⣿⣿⣿⣿⣿⠐⣠⡶⣶⣲⡎⢻⣿⣤⣴⣾⣿⣿⣿⣿⣿⠸⣿⣿\r\n⣿⠟⣋⡥⡶⣞⡯⣟⣾⣺⢽⡧⣥⣭⣉⢻⣿⣿⣿⣿⣿⣆⢻⣿\r\n⡃⣾⢯⢿⢽⣫⡯⣷⣳⢯⡯⠯⠷⠻⠞⣼⣿⣿⣿⣿⣿⣿⡌⣿\r\n⣦⣍⡙⠫⠛⠕⣋⡓⠭⣡⢶⠗⣡⣶⡝⣿⣿⣿⣿⣿⣿⣿⣧⢹\r\n⣿⣿⣿⣿⣿⣿⣘⣛⣋⣡⣵⣾⣿⣿⣿⢸⣿⣿⣿⣿⣿⣿⣿⢸\r\n⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢸⣿⣿⣿⣿⣿⣿⣿⢸\r\n⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢸⣿⣿⣿⣿⣿⣿⣿⢸");
        }
        public void Welcome()
        {
            _logger.Log("Welcome to the Goose Game!");
            _logger.LogEmptyLine();
            _logger.Log("⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿\r\n⣿⣿⣿⣿⣿⣿⣿⣿⡿⢋⣩⣭⣶⣶⣮⣭⡙⠿⣿⣿⣿⣿⣿⣿\r\n⣿⣿⣿⣿⣿⣿⠿⣋⣴⣿⣿⣿⣿⣿⣿⣿⣿⣿⣦⡙⢿⣿⣿⣿\r\n⣿⣿⣿⣿⣿⡃⠄⠹⡿⣿⣿⣿⣿⠟⠛⣿⣿⣿⣿⣷⡌⢿⣿⣿\r\n⣿⣿⣿⣿⣿⠐⣠⡶⣶⣲⡎⢻⣿⣤⣴⣾⣿⣿⣿⣿⣿⠸⣿⣿\r\n⣿⠟⣋⡥⡶⣞⡯⣟⣾⣺⢽⡧⣥⣭⣉⢻⣿⣿⣿⣿⣿⣆⢻⣿\r\n⡃⣾⢯⢿⢽⣫⡯⣷⣳⢯⡯⠯⠷⠻⠞⣼⣿⣿⣿⣿⣿⣿⡌⣿\r\n⣦⣍⡙⠫⠛⠕⣋⡓⠭⣡⢶⠗⣡⣶⡝⣿⣿⣿⣿⣿⣿⣿⣧⢹\r\n⣿⣿⣿⣿⣿⣿⣘⣛⣋⣡⣵⣾⣿⣿⣿⢸⣿⣿⣿⣿⣿⣿⣿⢸\r\n⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢸⣿⣿⣿⣿⣿⣿⣿⢸\r\n⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⢸⣿⣿⣿⣿⣿⣿⣿⢸");
            _logger.LogEmptyLine();
        }
        private void ShowPositions()
        {
            foreach (var player in _currentPlayers)
            {
                _logger.Log($"{player.Name} - Positie = {player.Position}");
                _logger.LogEmptyLine();
            }
        }
    }
}
