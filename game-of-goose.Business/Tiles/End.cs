﻿using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;

namespace game_of_goose.Business.Tiles
{
    public class End : Tile
    {
        public End(int position, ILogger logger) : base(position, logger)
        {

        }
        public override void CheckPosition(IPlayer player)
        {
            player.WinsGame(player);
        }
    }
}
