﻿using game_of_goose.Business.Logging;
using game_of_goose.Business.Players;

namespace game_of_goose.Business.Tiles
{
    public class Prison : Tile
    {
        public Prison(int position, ILogger logger) : base(position, logger)
        {

        }
        public override void CheckPosition(IPlayer player)
        {
            _logger.Log($"{player.Name} komt op plek {Position}. Je belandt in het gevang in Halle, sla 3 beurten over en laat je zeep niet vallen!");
            player.AddTurnCount(3);
        }
    }
}
