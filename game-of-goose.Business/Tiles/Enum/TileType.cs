﻿namespace game_of_goose.Business.Tiles.Enum
{
    public enum TileType
    {
        Bridge,
        Inn,
        Death,
        End,
        Maze,
        Prison,
        Well,
        Goose,
        RegularTile,
    }
}
